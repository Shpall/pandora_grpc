package main

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/spf13/afero"
	"google.golang.org/grpc"

	"github.com/fullstorydev/grpcurl"
	"github.com/yandex/pandora/cli"
	"github.com/yandex/pandora/components/phttp/import"
	"github.com/yandex/pandora/core"
	"github.com/yandex/pandora/core/aggregator/netsample"
	"github.com/yandex/pandora/core/import"
	"github.com/yandex/pandora/core/register"
)

type Ammo struct {
	Tag string
	Svc string
	Data map[string]interface{}
}

type GunConfig struct {
	Target string `validate:"required"` // Configuration will fail, without target defined
	ProtosetDir string
	ProtosetExt string
}

type Gun struct {
	// Configured on construction.
	client grpc.ClientConn
	conf   GunConfig
	// Configured on Bind, before shooting
	aggr core.Aggregator // May be your custom Aggregator.
	core.GunDeps
	protoset grpcurl.DescriptorSource
}

func GunFactory(conf GunConfig) *Gun {
	return &Gun{conf: conf}
}

func GunConfigFactory() GunConfig {
	return GunConfig{
		Target: "default target",
		ProtosetDir: "protosets",
		ProtosetExt: ".pb",
	}
}

func pathWalker(ext string, files *[]string) filepath.WalkFunc {
	return func(file string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		if filepath.Ext(file) != ext {
			return nil
		}
		*files = append(*files, file)
		return nil
	}
}

func findProtosetFiles(path string, ext string) ([]string, error) {
	var files []string
	err := filepath.Walk(path, pathWalker(ext, &files))
	if err != nil {
		return nil, err
	}
	return files, nil
}

func (g *Gun) Bind(aggr core.Aggregator, deps core.GunDeps) error {
	files, err := findProtosetFiles(g.conf.ProtosetDir, g.conf.ProtosetExt)
	if err != nil {
		return err
	}

	protoset, err := grpcurl.DescriptorSourceFromProtoSets(files...)
	if err != nil {
		return err
	}

	dialTime := time.Second
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, dialTime)
	defer cancel()
	cc, err := grpcurl.BlockingDial(ctx, "tcp", g.conf.Target, nil, nil...)
	if err != nil {
		return err
	}

	g.client = *cc
	g.aggr = aggr
	g.GunDeps = deps
	g.protoset = protoset
	return nil
}

func (g *Gun) doRequest(ammo *Ammo, opts ...grpc.CallOption) int {
	code := 0

	data, err := json.Marshal(ammo.Data)
	if err != nil {
		log.Printf("FATAL: %s", err)
		return 500
	}

	in := bytes.NewReader(data)
	rf, formatter, err := grpcurl.RequestParserAndFormatterFor(grpcurl.Format("json"), g.protoset, false, true, in)
	if err != nil {
		log.Printf("FATAL: %s", err)
		return 500
	}

	h := grpcurl.NewDefaultEventHandler(ioutil.Discard, g.protoset, formatter, false)
	err = grpcurl.InvokeRPC(context.TODO(), g.protoset, &g.client, ammo.Svc, nil, h, rf.Next)
	if err != nil {
		log.Printf("FATAL: %s", err)
		return 500
	}

	code = 200
	return code
}

func (g *Gun) Shoot(ammo core.Ammo) {
	customAmmo := ammo.(*Ammo)
	g.shoot(customAmmo)
}

func (g *Gun) shoot(ammo *Ammo) {
	code := 0
	sample := netsample.Acquire(ammo.Tag)

	code = g.doRequest(ammo)

	defer func() {
		sample.SetProtoCode(code)
		g.aggr.Report(sample)
	}()
}

func ammoCreator() core.Ammo {
	return &Ammo{}
}

func main() {
	//debug.SetGCPercent(-1)
	// Standard imports.
	fs := afero.NewOsFs()
	coreimport.Import(fs)
	// May not be imported, if you don't need http guns and etc.
	phttp.Import(fs)

	// Custom imports. Integrate your custom types into configuration system.
	coreimport.RegisterCustomJSONProvider("custom_provider", ammoCreator)

	register.Gun("pandora_grpc", GunFactory, GunConfigFactory)

	cli.Run()
}
