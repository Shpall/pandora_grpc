## Dynamic gRPC loadtest (Proof of concepts)

- compile your proto-files
    ```
    protoc --descriptor_set_out=descriptor.pb path/to/proto/files/*.proto
    ```
- put descriptor to `protosets/` directory
- generate ammo, for example
  ```
  echo '{"tag":"grpc","svc":"Service/method","data":{"someParam":1}}' > json.ammo 
  ```
- build generator
  ```
  go build pandora_grpc.go
  ```
- build docker image (see [Dockerfile](Dockerfile))
  ```
  # build basic image with:
  git clone https://github.com/yandex/yandex-tank.git && cd yandex-tank/docker && docker build --build-arg VERSION=develop --build-arg BRANCH=develop -t yatank:develop . && cd ../.. && rm -rf yandex-tank
  
  docker build --no-cache -t yatank_grpc
  ```
- run
  ```
  docker run -v $(pwd):/var/loadtest --net host yatank_grpc
  ```