module awesomeProject

go 1.12

require (
	github.com/fullstorydev/grpcurl v1.4.0
	github.com/golang/protobuf v1.3.2
	github.com/spf13/afero v1.2.2
	github.com/yandex/pandora v0.3.0
	google.golang.org/grpc v1.23.1
)
